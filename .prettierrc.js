module.exports = {
  printWidth: 90,
  tabWidth: 2,
  useTabs: false,
  singleQuote: false,
  semi: true,
  bracketSpacing: true,
};
